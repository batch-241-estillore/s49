// //mock database
// let posts = [];

// //Post ID
// let count = 1;
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((data) => showPosts(data))

//Reactive DOM with Fetch (CRUD Operation)

//ADD POST DATA

document.querySelector("#form-add-post").addEventListener('submit', (e) => {
	//Prevents the default behavior of an event
	//Used to prevent the page from reloading(the default behavior of submit)
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert("Post successfully added!");
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	});
});

//RETRIEVE POST
const showPosts = (posts) => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
		<div id="post-${post.id}">
		<h3 id="post-title-${post.id}">${post.title}</h3>
		<p id="post-body-${post.id}">${post.body}</p>
		<button onClick="editPost('${post.id}')">Edit</button>
		<button onClick="deletePost('${post.id}')">Delete</button>
		</div>
		`
	})
	console.log(postEntries);
	document.querySelector("#div-post-entries").innerHTML = postEntries;
};

//EDIT POST (Edit button)

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;
	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	document.querySelector("#btn-submit-update").removeAttribute("disabled")
};


//UPDATE POST (Update button)

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/100', {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 100
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Post successfully updated!')

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

		document.querySelector('#btn-submit-update').setAttribute('disabled', true)
	})

})

//DELETE POST (Delete button)

const deletePost = (id) => {
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: 'DELETE'
	})
	
	document.querySelector(`#post-${id}`).remove();
	alert('Post successfully updated!')
	;

};
